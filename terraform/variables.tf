variable "cidr_block_vpc" {}
variable "cidr_block_subnet" {}
variable "avail_zone" {}
variable "env_prefix" {}
variable "my_ip" {}
variable "instance_type" {}
variable "ssh_key" {}