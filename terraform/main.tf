provider "aws" {
  region = "eu-west-3"
}

# Create virtual private cloud for ec2
# enable dns public for ansible inventory aws_ec2
resource "aws_vpc" "app_vpc" {
  cidr_block = var.cidr_block_vpc
  enable_dns_hostnames = true
  tags = {
    Name: "${var.env_prefix}-vpc"
  }
}

# Create subnet and connect to virtual private cloud
resource "aws_subnet" "app_sub" {
  cidr_block = var.cidr_block_subnet
  vpc_id = aws_vpc.app_vpc.id
  availability_zone = var.avail_zone
  tags = {
    Name: "${var.env_prefix}-subnet-1"
  }

}

# Create route table for gateway to the external network
resource "aws_route_table" "app_route" {
  vpc_id = aws_vpc.app_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.app_gateway.id
  }
  tags = {
    Name: "${var.env_prefix}-routeTable"
  }
}

# Create internet gateway
resource "aws_internet_gateway" "app_gateway" {
  vpc_id = aws_vpc.app_vpc.id
  tags = {
    Name: "${var.env_prefix}-gateway"
  }
}

# Append your subnet to the route table and be as a main route

resource "aws_route_table_association" "app_route_association" {
  route_table_id = aws_route_table.app_route.id
  subnet_id = aws_subnet.app_sub.id
}

# Add  key pair to the cloud, use key from host
resource "aws_key_pair" "ssh-key" {
  key_name = "id_ed25519"
  public_key = file(var.ssh_key)
}

# Create Security group
# INPUT from your public ip to remote server port 22 SSH
# INPUT from any to remote server port 8080 >> connect to your website
# INPUT from your public ip to remote server port 8081 >> connect to phpMyAdmin
# OUTPUT from remote server to any port openAll
resource "aws_default_security_group" "app_sg" {
  vpc_id = aws_vpc.app_vpc.id

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8081
    protocol  = "tcp"
    to_port   = 8081
    cidr_blocks = [var.my_ip]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name: "${var.env_prefix}-sg"
  }
}

# export date from aws about instance latest image
# Owner: amazon

data "aws_ami" "app_image" {
  most_recent = true
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
  owners = ["137112412989"]
}
# Create instance
# Instance type: t2.micro

resource "aws_instance" "app_server" {
  ami = data.aws_ami.app_image.id
  instance_type = var.instance_type
  subnet_id = aws_subnet.app_sub.id
  vpc_security_group_ids = [aws_default_security_group.app_sg.id]
  availability_zone = var.avail_zone
  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.id

  tags = {
    Name: "${var.env_prefix}-server"
  }
}

output "ec2_public_ip" {
  value = aws_instance.app_server.public_ip
}
