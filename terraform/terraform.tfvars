cidr_block_vpc = "10.0.0.0/16"
cidr_block_subnet = "10.0.0.0/24"
avail_zone = "eu-west-3a"
env_prefix = "dev"
instance_type = "t2.micro"
ssh_key = "" # Add your ssh key