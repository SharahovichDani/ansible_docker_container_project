# Automation deployment application

## Introduction

It is an automatic deployment program, based on Terraform, Ansible, and Docker-Compose.

Using Docker compose, you can deploy applications on newly created EC2 instances.


## Setup

1. Clone the repo with ```git clone https://gitlab.com/SharahovichDani/ansible_docker_container_project.git``` 

2. Connect your AWS account with  ```aws configure```

3. Switch to the project's root directory in terminal.

4. Switch to the terraform directory and add to terraform.tfvars your ssh_key directory file ```ssh_key = "/root/.ssh/rsa.pem" # Add your ssh key```

5. Apply terraform ```terraform apply -auto--approve``` in terraform directory.

6. Add to ansible.cfg the same ssh key directory file as in section 4 

7. Add to docker_vars your authentication details to connect to Docker hub 

```
docker_user: user
docker_password: password
```

8. Apply ansible play-book ```ansible-playbook docker_deploy.yaml```

* If you want to use your own Docker compose file change it in ```cd roles/start_containers/files/```  with name docker-compose.yaml




## Tools Requirements

* Terraform

* Ansible

* Python3: Moudles boto3, botocore

* AWS Cli

* Docker hub Account 




